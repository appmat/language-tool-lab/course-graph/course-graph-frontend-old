import {SpecialtyItems, FacultyItems} from "../Items"

export const fetchFaculties = (url) => {
    return {
        type: "FETCH_DATA_FACULTIES",
        faculties: FacultyItems
    };
};

export const fetchSpecialties = (url) => {
    return {
        type: "FETCH_DATA_SPECIALTIES",
        specialties: SpecialtyItems
    };
};
















/*export function fetchFacultiesSuccess(faculties){
    console.log('got faculties!');
    return {
        type: 'FETCH_DATA_FACULTIES',
        faculties
    }
}*/

/*export function fetchSpecialtiesSuccess(specialties){
    return {
        type: "FETCH_DATA_SPECIALTIES",
        specialties: specialties
    };
}*/

/* export function fetchFaculties(url) {
    /*return (dispatch) => {
        fetch(url)
            .then(res=>{
                if (!res.ok){
                    throw new Error(res.statusText);
                }
                return res;
            })
            .then(res=>res.json())
            .then(faculties=>dispatch(fetchFacultiesSuccess(faculties)))
            .catch();
    }*/
    /*let faculties=FacultyItems
    return (dispatch) => {
        dispatch(fetchFacultiesSuccess(faculties))
    }
}*/

/*export function fetchSpecialties(url) {
    /*return (dispatch) => {
        fetch(url)
            .then(res=>{
                if (!res.ok){
                    throw new Error(res.statusText);
                }
                return res;
            })
            .then(res=>res.json())
            .then(specialties=>dispatch(fetchSpecialtiesSuccess(specialties)))
            .catch();
    }*/
    /*let specialties=SpecialtyItems
    return (dispatch) => {
        dispatch(fetchSpecialtiesSuccess(specialties))
    }
}*/

