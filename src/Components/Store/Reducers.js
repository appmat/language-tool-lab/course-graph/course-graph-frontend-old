const initialState={
    faculties: [],
    specialties: []
}

export const rootReducer = (state=initialState, action) => {
    switch (action.type) {
        case "FETCH_DATA_FACULTIES":
            return {...state, faculties: action.faculties};
        case "FETCH_DATA_SPECIALTIES":
            return {...state, specialties: action.specialties};
        default:
            return state;
    }
}