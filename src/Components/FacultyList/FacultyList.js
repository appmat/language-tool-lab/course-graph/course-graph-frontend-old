import React from 'react';
import './FacultyList.css';
import { connect } from 'react-redux';
import { fetchFaculties } from '../Store/Actions';
import { bindActionCreators } from "redux";

import SpecialtyList from "./SpecialtyList/SpecialtyList";
//import ModalChangeNameFac from "./ModalChangeNameFac/ModalChangeNameFac";

class FacultyList extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            //show: false,
            data: []
        };
    }
    /*
        showModal = () => {
            this.setState({
                show: !this.state.show
            });
        }


        * <li>
            <ModalChangeNameFac
                id={id}
                show={this.state.show}
                str={this.state.name}
                onClose={this.showModal}
            />
            <button
                className="edit"
                onClick={!this.state.show ? (e) => {
                    this.showModal()
                } : null}>
                <i className="fas fa-pen"/>
            </button>
        </li>
        * */

    componentDidMount() {
        this.props.fetchFaculties('');
    }

    render() {
        const fac=this.props.faculties;
        return(
            <div className="specialtyPage">
                {fac.map((item)=>{
                    let id=item.id;
                    let name=item.name;
                    return(
                        <div key={id}>
                            <ul className="title-institute">
                                <li>{name}</li>
                            </ul>
                            <SpecialtyList id={id}/>
                        </div>
                    )
                })}
                <p/>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return{
        faculties: state.faculties
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        fetchFaculties: bindActionCreators(fetchFaculties, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FacultyList)