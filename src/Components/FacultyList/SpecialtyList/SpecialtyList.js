import React from 'react';
import './SpecialtyList.css';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { fetchSpecialties } from '../../Store/Actions';
import { bindActionCreators } from "redux";

class SpecialtyList extends React.Component{
    componentDidMount() {
        this.props.fetchSpecialties('');
    }

    getSpecialtyById(){
        const spec=this.props.specialties;
        let data=[];
        spec.forEach(item=>{
            if (item.fac_id===this.props.id) {
                data.push(item)
            }
        })
        return data;
    }

    render() {
        const spec=this.getSpecialtyById();
        return(
            <div>
                <ul className="specialty-list">
                    {spec.map((item)=>{
                            return(
                                <Link key={item.id} to={`/specialty/${item.id}`} className="specialty-name">
                                    <div className="specialty-panel">
                                        {item.name}
                                    </div>
                                </Link>
                            )
                        })}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        specialties: state.specialties
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        fetchSpecialties: bindActionCreators(fetchSpecialties, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SpecialtyList)