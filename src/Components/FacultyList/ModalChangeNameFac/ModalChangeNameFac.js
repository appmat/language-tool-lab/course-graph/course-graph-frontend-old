import React from "react";

class ModalChangeNameFac extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            value: '',
            error: false
        }
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        this.setState({value: e.target.value});
    }
    onClose=e=>{
        this.setState({
            error: false,
            value: ''
        })
        this.props.onClose && this.props.onClose(e)
    }

    onChange=e=>{
        if (this.state.value===''){
            this.setState({
                error: true
            })
        }else{
            this.props.onClose && this.props.onClose(e);
            alert('Name was changed to ' + this.state.value);
            this.setState({
                error: false,
                value: ''
            })
        }
    }

    render(){
        if (!this.props.show){
            return null;
        }
        return (
            <div className="modal-change-name">
                <div className="modal-main-change-name">
                    <p className="task-change-name">Изменить название института с id={this.props.id}</p>
                    <input
                        type="text"
                        value={this.state.value}
                        onChange={this.handleChange}
                        className="input-change-name"
                        placeholder="Новое название"
                    />
                    <p className="error-specialty" style={this.state.error ? {visibility: 'visible'} : {visibility: 'hidden'}}> Поле должно быть непустым </p>

                    <div className="button-container-specialty">
                        <ul className="button-list">
                            <li>
                                <button
                                    className="button-change-name cancel"
                                    onClick={e=>{this.onClose(e);}}
                                >
                                    Отмена
                                </button>
                            </li>

                            <li>
                                <button className="button-change-name" onClick={e=>{this.onChange(e);}}>
                                    Подтвердить
                                </button>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>
        );
    }
}

export default ModalChangeNameFac