export const FacultyItems=[
    {
        "id": 1,
        "name": "ИВТИ"
    },
    {
        "id": 2,
        "name": "ИРЭ"
    },
    {
        "id": 3,
        "name": "ИТАЭ"
    },
    {
        "id": 4,
        "name": "ГПИ"
    }
]

export const SpecialtyItems=[
    {
        "id": 125,
        "name": "Прикладная математика и информатика",
        "fac_id": 1
    },
    {
        "id": 126,
        "name": "Математическое моделирование",
        "fac_id": 1
    },
    {
        "id": 127,
        "name": "Еще что то",
        "fac_id": 3
    },
    {
        "id": 128,
        "name": "Еще что то",
        "fac_id": 2
    },
    {
        "id": 129,
        "name": "Еще что то",
        "fac_id": 3
    },
    {
        "id": 130,
        "name": "Еще что то",
        "fac_id": 1
    },
    {
        "id": 131,
        "name": "Еще что то",
        "fac_id": 4
    },
    {
        "id": 131,
        "name": "Еще что то",
        "fac_id": 1
    },
    {
        "id": 132,
        "name": "Еще что то",
        "fac_id": 2
    },
    {
        "id": 133,
        "name": "Еще что то",
        "fac_id": 2
    },
    {
        "id": 134,
        "name": "Еще что то",
        "fac_id": 2
    },
    {
        "id": 135,
        "name": "Еще что то",
        "fac_id": 4
    },
    {
        "id": 136,
        "name": "Еще что то",
        "fac_id": 2
    },
    {
        "id": 137,
        "name": "Еще что то",
        "fac_id": 3
    },
    {
        "id": 138,
        "name": "Еще что то",
        "fac_id": 2
    },
    {
        "id": 139,
        "name": "Еще что то",
        "fac_id": 4
    },
    {
        "id": 140,
        "name": "Еще что то",
        "fac_id": 1
    },
    {
        "id": 141,
        "name": "Еще что то",
        "fac_id": 4
    },
    {
        "id": 142,
        "name": "Еще что то",
        "fac_id": 2
    },
    {
        "id": 143,
        "name": "Еще что то",
        "fac_id": 2
    },
    {
        "id": 144,
        "name": "Еще что то",
        "fac_id": 2
    }

]

export const CourseItems=[
    {
        "id": 1,
        "name": "Физика"
    },
    {
        "id": 2,
        "name": "Математический анализ"
    },
    {
        "id": 3,
        "name": "Дифференциальные уравнения"
    },
    {
        "id": 4,
        "name": "Линейная алгебра и аналитическая геометрия"
    },
    {
        "id": 5,
        "name": "Теория графов и комбинаторика"
    },
    {
        "id": 6,
        "name": "Иностранный язык"
    },
    {
        "id": 7,
        "name": "Языки и методы программирования"
    },
    {
        "id": 8,
        "name": "Компьютерная графика"
    },
    {
        "id": 9,
        "name": "Системное программирование"
    },
    {
        "id": 10,
        "name": "Математическая логика"
    },
    {
        "id": 11,
        "name": "Численные методы"
    },
    {
        "id": 12,
        "name": "Функциональный анализ"
    },
    {
        "id": 13,
        "name": "Технология программирования"
    }
]

