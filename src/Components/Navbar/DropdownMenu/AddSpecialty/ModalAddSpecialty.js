import React from "react";
import './ModalAddSpecialty.css';


class ModalAddSpecialty extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            value2: 'Институт',
            value: '',
            error: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeSelect = this.handleChangeSelect.bind(this);
    }

    handleChangeSelect(event) {
        this.setState({value2: event.target.value2});
    }

    handleChange(e) {
        this.setState({value: e.target.value});
    }

    onClose = (e) => {
        this.props.onClose && this.props.onClose(e);
    }

    render(){
        if (!this.props.show){
            return null;
        }

        return (
            <div className="modal-change-name">
                <div className="modal-main-change-name" style={{height: "220px"}}>
                    <p className="task-change-name">Добавить специальность</p>
                    <form>
                        <select value={this.state.value2} onChange={this.handleChangeSelect} className="faculties-dropdown">
                            <option style={{color: "grey"}}>Выберите институт</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>

                        </select>
                        <input
                            type="text"
                            value={this.state.value}
                            onChange={this.handleChange}
                            className="input-change-name"
                            placeholder="Название"
                        />
                        <p className="error-specialty" style={this.state.error ? {visibility: 'visible'} : {visibility: 'hidden'}}> Поле должно быть непустым </p>
                    </form>
                    <ul className="button-list">
                        <li>
                            <button
                                className="button-change-name cancel"
                                onClick={(e) => {this.onClose(e);}}
                            >
                                Отмена
                            </button>
                        </li>

                        <li>
                            <button className="button-change-name" onClick={(e) => {this.onClose(e);}}>
                                Добавить
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default ModalAddSpecialty;