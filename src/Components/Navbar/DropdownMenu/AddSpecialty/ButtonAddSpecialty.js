import React from "react";
import ModalAddSpecialty from "./ModalAddSpecialty"

class ButtonAddSpecialty extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            showAddSpecialty: false,
        }
    }

    showAddSpecialty = () => {
        this.setState({
            showAddSpecialty: !this.state.showAddSpecialty
        });
    }

    render() {
        return(
            <div>
                <ModalAddSpecialty
                    show={this.state.showAddSpecialty}
                    onClose={this.showAddSpecialty}
                />
                <button className="button-dropdown"
                        onClick={!this.state.showAddSpecialty ? (e) => {
                            this.showAddSpecialty();
                        } : null }>
                    Добавить специальность
                </button>
            </div>
        )
    }

}

export default ButtonAddSpecialty;