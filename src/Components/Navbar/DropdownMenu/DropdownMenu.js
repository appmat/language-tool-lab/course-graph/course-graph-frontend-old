import React from 'react';
import { NavLink } from "react-router-dom";
import './DropdownMenu.css';
import ButtonAddFaculty from "./AddFaculty/ButtonAddFaculty";
import ButtonAddSpecialty from "./AddSpecialty/ButtonAddSpecialty";

class DropdownMenu extends React.Component{
    container = React.createRef();
    state = {
        open: false,
    };

    handleButtonClick = () => {
        this.setState(state => {
            return {
                open: !state.open,
            };
        });
    };

    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutside);
    }
    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutside);
    }
    handleClickOutside = event => {
        if (this.container.current && !this.container.current.contains(event.target)) {
            this.setState({
                open: false,
            });
        }
    };

    render() {
        return(
            <div className="container-dropdown" ref={this.container}>
                <button
                    onClick={this.handleButtonClick}
                    className="btn-plus"
                    style={
                        this.state.open ?
                            {backgroundColor: '#FAFAFA', boxShadow: '0 2px 2px rgba(0, 0, 0, 0.1)'}
                            : {}}
                >
                    <i className="fas fa-plus"/>
                </button>

                {this.state.open && (
                    <div className="dropdown">
                        <ul className="ul-dropdown">
                            <li className="li-dropdown">
                                <ButtonAddFaculty closeDropdown={this.handleButtonClick}/>
                            </li>
                            <li className="li-dropdown">
                                <ButtonAddSpecialty/>
                            </li>
                            <li className="li-dropdown">
                                <button className="button-dropdown">Добавить курс</button>
                            </li>

                            <li className="divider-dropdown">__________________________________</li>
                            <NavLink exact to="/" className="link" activeStyle={{ color: "grey", pointerEvents: 'none' }}
                                     onClick={this.handleButtonClick}> <li className="li-dropdown links">Все институты</li> </NavLink>
                            <NavLink to="/courses" className="link" activeStyle={{ color: "grey", pointerEvents:'none'}}
                                     onClick={this.handleButtonClick}> <li className="li-dropdown links">Все курсы</li> </NavLink>
                        </ul>
                    </div>
                )}
            </div>

        )
    }
}
export default DropdownMenu