import React from "react";
import './ModalAddFaculty.css';

class ModalAddFaculty extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            value: '',
            error: false
        }
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        this.setState({value: e.target.value});
    }

    onClose = (e) => {
        this.props.onClose && this.props.onClose(e);
    }

    render(){
        if (!this.props.show){
            return null;
        }
        console.log("hi im modal")
        return (
            <div className="modal-change-name">
                <div className="modal-main-change-name">
                    <p className="task-change-name">Добавить институт</p>
                    <input
                        type="text"
                        value={this.state.value}
                        onChange={this.handleChange}
                        className="input-change-name"
                        placeholder="Название"
                    />
                    <p className="error-specialty" style={this.state.error ? {visibility: 'visible'} : {visibility: 'hidden'}}> Поле должно быть непустым </p>

                    <ul className="button-list">
                        <li>
                            <button
                                className="button-change-name cancel"
                                onClick={(e) => {this.onClose(e);}}
                            >
                                Отмена
                            </button>
                        </li>

                        <li>
                            <button className="button-change-name" onClick={(e) => {this.onClose(e);}}>
                                Добавить
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default ModalAddFaculty;