import React from "react";
import ModalAddFaculty from './ModalAddFaculty'

class ButtonAddFaculty extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            showAddFaculty: false,
        }
    }

    showAddFaculty = () => {
        console.log('зашел');
        console.log(this.state.showAddFaculty);
        this.setState({
            showAddFaculty: !this.state.showAddFaculty
        });
        console.log('поменял');
        console.log(this.state.showAddFaculty);
        console.log('вышел');
    }

    closeDropdown = (e) => {
        console.log('закрыл');
        this.props.closeDropdown && this.props.closeDropdown(e);
    }

    render() {
        return(
            <div>
                <ModalAddFaculty
                    show={this.state.showAddFaculty}
                    onClose={this.showAddFaculty}
                />
                <button className="button-dropdown"
                        onClick={!this.state.showAddFaculty ? (e) => {
                            this.showAddFaculty(); /*this.closeDropdown(e);*/
                        } : null}>
                    Добавить институт
                </button>
            </div>
        )
    }

}

export default ButtonAddFaculty;