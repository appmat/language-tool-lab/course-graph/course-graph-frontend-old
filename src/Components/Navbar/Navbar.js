import React from 'react';
import './Navbar.css';
import DropdownMenu from './DropdownMenu/DropdownMenu'

class Navbar extends React.Component{
    render(){
        return(
            <div>
                <nav className="navbar-items">
                    <ul className="navbar-menu">
                        <li> <div className="navbar-search"> </div> </li>
                        <li> <DropdownMenu/> </li>
                        <li> <button className="navbar-btn">Войти</button> </li>
                    </ul>
                </nav>

            </div>
        )
    }
}


export default Navbar