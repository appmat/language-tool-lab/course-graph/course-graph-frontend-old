import React, {Component} from 'react';
import './CourseList.css'
import ModalChangeNameSpec from "./ModalChangeNameSpec/ModalChangeNameSpec";

import {CourseItems} from  "../Items"
import {Link} from "react-router-dom";

class CourseList extends Component{
    constructor(props) {
        super(props);
        this.state={
            show: false,
            //name: 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
            name: 'Какая-то специальность'
        };
    }

    showModal = () => {
        this.setState({
            show: !this.state.show
        });
    }

    render() {
        const {name}=this.state;
        return (
            <div className="coursePage">

                <ul className="title">
                    <li>
                        <Link to="/">
                            <button className="go-back">Назад</button>
                        </Link>
                    </li>
                    <li><p className="name-course">{name}</p></li>
                    <li>
                        <ModalChangeNameSpec
                            show={this.state.show}
                            str={this.state.name}
                            onClose={this.showModal}
                        />
                        <button
                            className="edit"
                            onClick={!this.state.show ? (e) => {
                                this.showModal()
                            } : null}>
                            <i className="fas fa-pen"/>
                        </button>
                    </li>
                </ul>

                <ul className="course-list">
                    {CourseItems.map((item) => {
                        return (
                            <div key={item.id} className="course-panel">
                                {item.name}
                            </div>
                        )
                    })}
                    <button className="course-panel add-panel">
                        <i className="fas fa-plus add-icon"/>
                    </button>
                </ul>
            </div>
            );
    }

}

export default CourseList;