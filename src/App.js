import './App.css';
import React from "react";
import Navbar from "./Components/Navbar/Navbar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Faculties from "./Pages/Faculties";
import CoursesFromSpecialty from "./Pages/CoursesFromSpecialty";
import AllCourses from "./Pages/AllCourses";

function App() {
  return (
    <div className="App">
        <Router>
            <Navbar />
            <Switch>
                <Route exact path="/" component={Faculties}/>
                <Route exact path={'/specialty/:id'} component={CoursesFromSpecialty}/>
                <Route exact path="/courses" component={AllCourses}/>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
