import React from 'react';
import CourseList from "../Components/CourseList/CourseList";
import { useParams } from "react-router-dom";

function CoursesFromSpecialty() {
    let {id} = useParams();
    return(
        <div>
            <CourseList id={id}/>
        </div>
    );
}

export default CoursesFromSpecialty;