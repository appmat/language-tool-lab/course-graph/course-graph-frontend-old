import React from 'react';
import Courses from '../Components/Courses/Courses';

function AllCourses() {
    return (
        <div>
            <Courses/>
        </div>
    );
}

export default AllCourses